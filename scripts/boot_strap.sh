#!/bin/bash
mkdir -p /home/vagrant/.ssh
echo ${ANSIBLE_PUB} >> /home/vagrant/.ssh/authorized_keys

# Install Git, Docker and Docker Compose
echo "Install Docker"
sudo yum install -y sudo yum-utils git
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Install Python 3.9 and ansible
echo "Install Python and Ansible"
sudo yum remove -y python3
sudo yum install -y python3.9
sudo pip3 install --upgrade pip

sudo systemctl start docker
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

sudo yum install -y bash-completion
sudo curl -L https://raw.githubusercontent.com/docker/machine/v0.16.0/contrib/completion/bash/docker-machine.bash -o /etc/bash_completion.d/docker-machine
