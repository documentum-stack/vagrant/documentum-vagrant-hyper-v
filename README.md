# Documentum-Vagrant-Hyper-V

This guide is applicable for Windows-10/11 using Hyper-V. If you are using VirtualBox/VMWare etc. Please feel free to fork and customise accordingly.

## Getting started

Please ensure that Hyper-V Networking is setup appropriately for inter-server-communications. Please follow below simple steps to create VM on Hyper-V (local/remote)

1. Fork the Project.
2. Enable Hyper-V on Windows-10/11. Please follow official guides.
3. Install Vagrant and Git-Bash. Please follow official guides.
4. Clone the Project in VS Code.
5. Open *Bash* Terminal in VS Code.
6. Export ansible user's Public Key. E.g.,

   ```bash
   export ANSIBLE_PUB=`cat ../gitlab-runner-vagrant/ansible.pub`
   ```

    **Note:** When you'll run `vagrant up` on `gitlab-runner-vagrant` project, you'll get `ansible.pub` file.

7. Run `vagrant up`.
8. Review the logs.

## Releases

- [ ] [V1-CENTOS-8](https://gitlab.com/TBC)

***
